GKFog Of War
============


.. raw:: html

   <iframe src="https://giphy.com/embed/EuxyC6kwt1mmsRPsTq" width="720" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

Features
--------

Versatile fog of war implementation;

* Fully Replicated
    * Conditional Replication for clients: Enemy actors are only replicated if it is visible
    * Cheat prevention: clients do not know about enemy actors until visible
* Dynamic
    * Faction change
    * Obstacle
    * Unlimited Number of Teams
* Fast C++ Implementation
    * Multithreaded
    * Fully exposed to blueprints
    * Fully extendable in C++ and Blueprints
* 128 Height level
* Configurable Fog Resolution
* Examples included
    * Minimap
    * Full Playable demo


Useful Links
------------

* `Playable demo <https://setepenre.itch.io/gkfogofwar>`_
* `Bug Tracker <https://gitlab.com/mischievousgame/gkfogofwar/-/issues>`_
* `Documentation <https://mischievousgame.gitlab.io/gkfogofwar/>`_
* `Youtube Channel <https://www.youtube.com/@mischievousgame>`_
* `Discord Server <https://discord.gg/eqb4Egv9b3>`_
* `Patreon <https://www.patreon.com/setepenre>`_
* `Twitter <https://twitter.com/SetepenreKit>`_
