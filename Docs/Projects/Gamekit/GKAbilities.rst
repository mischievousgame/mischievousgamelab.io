GKAbilities
===========

.. image:: https://github.com/Delaunay/Gamekit/blob/master/Docs/_static/Badges/DiscordBanner.png?raw=true
   :target: https://discord.gg/9d2rYraF
   :height: 18
   :alt: Discord

Goals
~~~~~

High level game framework built on top of Unreal Engine.

* Data Driven: focus on the game design
   * Configure your ability in json
   * Bootstrap Game wikis with the raw data

* Multiplayer Ready
   * Hosted
   * Dedicated
   * Standalone

* Ever growing documentation
   * Architecture Diagrams
   * Replication Diagrams
   * Code Search

* Community Friendly
   * Favour source code (C++, HLSL) for easier contribution workflow

* Base UI & Menus for debugging and prototyping

* Automation framework on top of UAT
   * CI/CD
   * Testing Utilities

.. image:: https://github.com/Delaunay/Gamekit/blob/master/Docs/_static/Tests/GamekitPlayground.PNG?raw=true


Features
~~~~~~~~

* Multiplayer Ready
* Everything is an Ability
   * Movement
   * Item
   * Skills
* Abilities are animation/skeleton independent
* Ability Queue
* 54 Gameplay Tags to implement a wide range of gameplay effects
* Configurable Ability from a single json file
* Tooltip generator
* Target Selection Actor
* Widgets
   * Ability Widget
   * Active Effect Widget
   * Ability Queue Widget
   * Inventory Widget
   * Equipment Widget
   * Minimap Widget
* Fog of War
* Animation Sets
   * Use multiple animations for the same action

* TopDown Camera Movements
   * Edge Pan Camera
   * Camera Grab
   * Directional Move

Releases
--------

* Latest Release

  * `Instruction`_

  * `Documentation`_


.. _`Instruction`: http://www.gamekit.ca/Welcome/Installation.html
.. _`Documentation`: http://www.gamekit.ca
