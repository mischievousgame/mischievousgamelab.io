Mischievous Game
================

The goal of mischievous is to implement small, self contained, reusable and replicated plugins which will 
culminate with the implementation of a full MOBA/RTS Game.


What our implementation strive for
----------------------------------

* Fast 
* Plug'n play
* Replicated

   * Standalone
   * ListenServer
   * Dedicated


Follow our journey
------------------

* `Documentation <https://mischievousgame.gitlab.io/gamekit/>`_
* `Youtube Channel <https://www.youtube.com/@mischievousgame>`_
* `Discord Server <https://discord.gg/eqb4Egv9b3>`_
* `Patreon <https://www.patreon.com/setepenre>`_
* `Twitter <https://twitter.com/SetepenreKit>`_

